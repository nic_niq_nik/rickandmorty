//
//  CharecterModel.swift
//  RickAndMorty
//
//  Created by Nithi Kulasiriswatdi on 23/7/2564 BE.
//

import Foundation

struct CharecterReponse: Codable {
    let info: ResponseInfo
    let results: [Charecter]
}

struct ResponseInfo: Codable {
    let count: Int
    let pages: Int
    let next: String
    let prev: String?
}

struct Charecter: Codable {
    let id: Int
    let name: String
    let status: String
    let species: String
    let type: String
    let gender: String
    let origin: Origin
    let image: String
    let episode: [String]
}

struct Origin: Codable {
    let name: String
    let url: String
}
