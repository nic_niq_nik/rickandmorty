//
//  CharecterModel.swift
//  RickAndMorty
//
//  Created by Nithi Kulasiriswatdi on 23/7/2564 BE.
//

import Foundation

struct EpisodeReponse: Codable {
    enum CodingKeys: String, CodingKey {
       case id, name, episode, characters
       case airDate = "air_date"
     }
    let id: Int
    let name: String
    let airDate: String
    let episode: String
    let characters: [String]
}

