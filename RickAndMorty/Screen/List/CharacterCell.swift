//
//  CharacterCell.swift
//  RickAndMorty
//
//  Created by Nithi Kulasiriswatdi on 23/7/2564 BE.
//

import UIKit
import Kingfisher

class CharacterCell: UICollectionViewCell {
    @IBOutlet weak var charecterImage: UIImageView!
    func setup(imageURL: String) {
        let url = URL(string: imageURL)
        charecterImage.contentMode = .scaleAspectFill
        charecterImage?.kf.setImage(with: url)
    }
}
