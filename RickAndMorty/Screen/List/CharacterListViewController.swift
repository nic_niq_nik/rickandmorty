//
//  CharacterListViewController.swift
//  RickAndMorty
//
//  Created by Nithi Kulasiriswatdi on 23/7/2564 BE.
//

import UIKit
import Alamofire

class CharacterListViewController: UIViewController {

    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    private var chatecterList: [Charecter]?
    var episodeURL: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if episodeURL != nil {
            getListFromEpisode(episodeURL: episodeURL!)
        } else {
            getCharecters()
        }
        searchBar.isHidden = episodeURL != nil
    }
    private func getCharecters(withFilter name: String? = nil) {
        loadIndicator.startAnimating()
        let url = "https://rickandmortyapi.com/api/character/?name=" + (name ?? "" )
        AF.request(url,
                   method: .get,
                   encoding: JSONEncoding.default)
            .validate()
             .responseDecodable(of: CharecterReponse.self) { [weak self] response in
                 guard let self = self else { return }
                self.loadIndicator.stopAnimating()
                 switch response.result {
                 case .success(let result):
                    self.chatecterList = result.results
                    self.collectionView.reloadData()
                 case .failure(let error):
                    self.clearList()
                    print("fail:\(error) response: \(String(describing: response.data))")
                 }
             }
    }
    private func getListFromEpisode(episodeURL: String) {
        loadIndicator.startAnimating()
        AF.request(episodeURL,
                   method: .get,
                   encoding: JSONEncoding.default)
            .validate()
             .responseDecodable(of: EpisodeReponse.self) { [weak self] response in
                 guard let self = self else { return }
                 switch response.result {
                 case .success(let result):
                    self.getCharecterFromMultipleID(charecters: result.characters)
                 case .failure(let error):
                    print("fail:\(error) response: \(String(describing: response.data))")
                 }
             }
    }
    private func getCharecterFromMultipleID(charecters: [String]) {
        let ids = charecters.map{
            return $0.replacingOccurrences(of: "https://rickandmortyapi.com/api/character/", with: "")
        }.reduce(""){
            return "\($0),\($1)"
        }
        AF.request("https://rickandmortyapi.com/api/character/" + ids,
                   method: .get,
                   encoding: JSONEncoding.default)
            .validate()
             .responseDecodable(of: [Charecter].self) { [weak self] response in
                 guard let self = self else { return }
                self.loadIndicator.stopAnimating()
                 switch response.result {
                 case .success(let result):
                    self.chatecterList = result
                    self.collectionView.reloadData()
                 case .failure(let error):
                    print("fail:\(error) response: \(String(describing: response.data))")
                 }
             }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard  let destination = segue.destination as? CharecterDetailViewController,
               let charecter = sender as? Charecter else {
            return
        }
        destination.setupData(with: charecter)
    }
    private func clearList() {
        chatecterList = nil
        collectionView.reloadData()
    }
}
extension CharacterListViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        getCharecters(withFilter: searchBar.text)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        getCharecters(withFilter: searchBar.text)
        searchBar.resignFirstResponder()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        clearList()
    }
    
}
extension CharacterListViewController: UICollectionViewDelegate,
                                       UICollectionViewDataSource,
                                       UICollectionViewDelegateFlowLayout{
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chatecterList?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let minSpace: CGFloat = 10
        let itemPerRow: CGFloat = 3
        let width = (collectionView.bounds.width - minSpace * (itemPerRow+1))/itemPerRow
        let height = width
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "characterCellIdentifier",
                for: indexPath) as? CharacterCell ,
              let charecter = chatecterList?[indexPath.row] else {
            return UICollectionViewCell()
        }
        cell.setup(imageURL: charecter.image)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let charecter = chatecterList?[indexPath.row]
        self.performSegue(withIdentifier: "toCharecterDetailViewController",
                          sender: charecter)
    }
}
