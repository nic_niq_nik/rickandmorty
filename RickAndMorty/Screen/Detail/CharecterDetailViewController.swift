//
//  CharecterDetailViewController.swift
//  RickAndMorty
//
//  Created by Nithi Kulasiriswatdi on 23/7/2564 BE.
//

import UIKit

class CharecterDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var character: Charecter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        regisTableView()
    }
    func setupData(with model: Charecter) {
        character = model
    }
    
}
extension CharecterDetailViewController: UITableViewDelegate, UITableViewDataSource {
    private func regisTableView() {
        tableView.register(UINib(nibName: "CharecterHeaderView", bundle: nil),
                           forHeaderFooterViewReuseIdentifier: "Header")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return character?.episode.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = "\(indexPath.row + 1)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 160
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url = character?.episode[indexPath.row] else {
            return
        }
        performSegue(withIdentifier: "toCharacterListViewController", sender: url)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "Header") as? CharecterHeaderView,
              let character = character else {
            return nil
        }
        view.setupDisplay(with: character)
        return view
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.destination as? CharacterListViewController,
              let url = sender as? String else {
            return
        }
        vc.episodeURL = url
    }
}
