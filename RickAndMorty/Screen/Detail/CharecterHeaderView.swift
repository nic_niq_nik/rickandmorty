//
//  CharecterHeaderView.swift
//  RickAndMorty
//
//  Created by Nithi Kulasiriswatdi on 24/7/2564 BE.
//

import UIKit
import Kingfisher

class CharecterHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var charecterImageView: UIImageView?

    func setupDisplay(with model: Charecter) {
        let url = URL(string: model.image)
        contentView.backgroundColor = .white
        charecterImageView?.contentMode = .scaleAspectFill
        charecterImageView?.kf.setImage(with: url)
        titleLabel?.text = model.name
    }

}
